import "./App.css";

function App() {
  return (
    <div>
      <div className="container">
        <form action="" className="mx-auto mt-20">
          <div className="text-3xl text-success">
            <h2>Formulaire</h2>
          </div>
          <div>
            <label htmlFor="">Nom & Prenom</label>
            <input type="text" className="form-control" placeholder="" />
          </div>
          <div>
            <label htmlFor="">DateNaiss</label>
            <input type="Date" className="form-control" />
          </div>
          <div>
            <label htmlFor="">Telephone</label>
            <input type="tel" className="form-control" />
          </div>
          <div>
            <label htmlFor="">Profession</label>
            <input type="text" className="form-control" />
          </div>
          <div>
            <label htmlFor="">Email</label>
            <input type="email" className="form-control" />
          </div>
          <div>
            <label htmlFor="">Password</label>
            <input type="password" className="form-control" />
          </div>
          <div>
            <button className="btn btn-success btn-block mt-3">
              S'inscrire
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default App;
